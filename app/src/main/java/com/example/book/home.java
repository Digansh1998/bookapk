package com.example.book;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.example.book.R.id.lv2;

public class home extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    FloatingActionButton fab1;
    ListView l1,catelist;
    DataBaseHelper dbh;
    Cursor cursor;
    ArrayList<SingleRow> mylist;
    MyAdaptar adaptar;
    ArrayList Aname,Categories;
    NavigationView navigationView;
    Spinner spinner;
    List<Submenu> headerList = new ArrayList<>();
    HashMap<Submenu, List<Submenu>> childList = new HashMap<>();
    ExpandableListAdapter expandableListAdapter;
    ExpandableListView expandableListView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        l1 = findViewById(R.id.lv1);
        dbh=new DataBaseHelper(this);
        cursor=dbh.listdata();
        expandableListView = findViewById(R.id.lv2);
        prepareMenuData();
        populateExpandableList();

        mylist = new ArrayList<>();
        SingleRow singleRow;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Aname=new ArrayList();
        while (cursor.moveToNext())

        {
            String name=cursor.getString(0);
            byte[]image=cursor.getBlob(3);
            Aname.add(cursor.getString(0));


            singleRow = new SingleRow(name, image);
            mylist.add(singleRow);

        }
        adaptar=new MyAdaptar(this,mylist);
        l1.setAdapter(adaptar);
        l1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent=new Intent(home.this,list.class);
                intent.putExtra("Position",Aname.get(position).toString());
                startActivity(intent);
                // Toast.makeText(getApplicationContext(),"Toast",Toast.LENGTH_LONG).show();
            }
        });





        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        /*Categories=new ArrayList();
        //Categories.add("Categories");
        Categories.add("Art and Craft");
        Categories.add("Comics");
        Categories.add("Cooking");
        Categories.add("Health and Fitness");
        Categories.add("Spiritual");
        Categories.add("Study");
        Categories.add("Architecture");
        Categories.add("Medical");
        Categories.add("Novel");


        spinner = (Spinner) navigationView.getMenu().findItem(R.id.categories).getActionView();
        ArrayAdapter adapter=new ArrayAdapter(getApplicationContext(),android.R.layout.simple_spinner_dropdown_item,Categories);
        spinner.setAdapter(adapter);*/
       /* spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(),"Hello",Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

    }



   private void prepareMenuData() {
        Submenu submenu ;

        submenu=new Submenu("Categories",true,true,"");
        headerList.add(submenu);
       if (!submenu.hasChildren) {
           childList.put(submenu, null);
       }


        List<Submenu> childModelsList = new ArrayList<>();
        Submenu childModel = new Submenu("Art and Craft", false, false, "Art and Craft");
        childModelsList.add(childModel);
childModel=new Submenu("Comics",false,false,"Comics");
childModelsList.add(childModel);
       childModel=new Submenu("Cooking",false,false,"Cooking");
       childModelsList.add(childModel);
       childModel=new Submenu("Health and Fitness",false,false,"Health and Fitness");
       childModelsList.add(childModel);
       childModel=new Submenu("Spiritual",false,false,"Spiritual");
       childModelsList.add(childModel);
       childModel=new Submenu("Study",false,false,"Study");
       childModelsList.add(childModel);
       childModel=new Submenu("Architecture",false,false,"Architecture");
       childModelsList.add(childModel);
       childModel=new Submenu("Medical",false,false,"Medical");
       childModelsList.add(childModel);
       childModel=new Submenu("Novel",false,false,"Novel");
       childModelsList.add(childModel);
       childModel=new Submenu("Engineering",false,false,"Engineering");
       childModelsList.add(childModel);
      /* if (submenu.hasChildren) {
           Log.d("API123","here");
           childList.put(submenu, childModelsList);
       }*/
      if (submenu.hasChildren) {
           childList.put(submenu, childModelsList);
       }

    }
    private void populateExpandableList() {
        expandableListAdapter = new ExpandableListAdapter(this, headerList, childList);
        expandableListView.setAdapter(expandableListAdapter);
expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
    @Override
    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
        if (headerList.get(groupPosition).isGroup) {
            if (!headerList.get(groupPosition).hasChildren) {
                Toast.makeText(getApplicationContext(),headerList+"",Toast.LENGTH_LONG).show();
                onBackPressed();
            }
        }
        return false;
    }
});
expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        if (childList.get(headerList.get(groupPosition)) != null) {
            Submenu model = childList.get(headerList.get(groupPosition)).get(childPosition);
            if (model.categoriesname.length() > 0) {
                //Toast.makeText(getApplicationContext(),model.categoriesname+"",Toast.LENGTH_LONG).show();

                Intent intent=new Intent(getApplicationContext(),category.class);
                intent.putExtra("cate",model.categoriesname+"");
                startActivity(intent);
                onBackPressed();
            }
        }
        return false;
    }
});
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.search,menu);
        MenuItem item=menu.findItem(R.id.search);


        final SearchView searchView=(SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adaptar.getFilter().filter(newText);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_yourcontributions) {
            Toast.makeText(getApplicationContext(),"Your Contribition",Toast.LENGTH_LONG).show();
        } else if (id == R.id.nav_walkthrough) {
            Toast.makeText(getApplicationContext(),"Walkthrough",Toast.LENGTH_LONG).show();

        } else if (id == R.id.nav_share) {
            Toast.makeText(getApplicationContext(),"Share",Toast.LENGTH_LONG).show();

        } else if (id == R.id.nav_feedback) {
            Toast.makeText(getApplicationContext(),"Feedback",Toast.LENGTH_LONG).show();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    public void floatingactionbutton(View view)

    {
        fab1=(FloatingActionButton)findViewById(R.id.fab1);
        Intent intent=new Intent(home.this,Addbook.class);
        startActivity(intent);
    }
public void profil (View view)
{boolean p=false;
    if (p==true) {
        Intent intent = new Intent(getApplicationContext(), profile.class);
        startActivity(intent);
    }else if (p==false)
    {
       Intent intent=new Intent(getApplicationContext(),register.class);
       startActivity(intent);
    }

}



}
