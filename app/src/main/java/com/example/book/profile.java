package com.example.book;

import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class profile extends AppCompatActivity {
    DataBaseHelper dbh;
    ImageView iv1;
    TextView tv1,tv2,tv3,tv4;
    Cursor cursor;
    String Username;
    Bundle usname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        dbh=new DataBaseHelper(this);
        iv1=(ImageView)findViewById(R.id.imageView1);
        tv1=(TextView)findViewById(R.id.tv1);
        tv2=(TextView)findViewById(R.id.tv2);
        tv3=(TextView)findViewById(R.id.tv3);
        tv4=(TextView)findViewById(R.id.tv4);
        usname=getIntent().getExtras();
        Username=usname.get("username").toString();
        cursor=dbh.profil(Username);
        if (cursor.moveToNext())
        {
            iv1.setImageBitmap(BitmapFactory.decodeByteArray((byte[])cursor.getBlob(0),0,((byte[])cursor.getBlob(0)).length));
            tv1.setText(cursor.getString(1));
            tv2.setText(cursor.getString(3));
            tv3.setText(cursor.getString(4));
            tv4.setText(cursor.getString(5));

        }



    }

}
