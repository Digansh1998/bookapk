package com.example.book;

import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class list extends AppCompatActivity {
    DataBaseHelper dbh;
    Bundle B;
    String Position;
    ImageView iv1,iv2;
    TextView tv1,tv2;
    Cursor cursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        iv1=(ImageView)findViewById(R.id.iv1);
        iv2=(ImageView)findViewById(R.id.iv2);
        tv1=(TextView)findViewById(R.id.tv1);
        tv2=(TextView)findViewById(R.id.tv2);
        B=getIntent().getExtras();
        Position=B.get("Position").toString();
        //Toast.makeText(getApplicationContext(),Position+"",Toast.LENGTH_LONG).show();
        dbh=new DataBaseHelper(this);
        cursor=dbh.selectdata(Position);
        if (cursor.moveToNext())
        {
            iv1.setImageBitmap(BitmapFactory.decodeByteArray((byte[])cursor.getBlob(3),0,((byte[])cursor.getBlob(3)).length));
            tv1.setText(cursor.getString(0));
            tv2.setText(cursor.getString(1));
        }

    }
}
