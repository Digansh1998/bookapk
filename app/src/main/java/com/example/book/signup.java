package com.example.book;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;

public class signup extends AppCompatActivity {
    DataBaseHelper dbh;
    ImageView iv1;
    EditText et1,et2,et3,et4,et5;
    Button b1;
    byte[] byteArray;
    String username,password,email,mobil,pincode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        iv1=(ImageView)findViewById(R.id.imageView2);
        et1=(EditText)findViewById(R.id.et1);
        et2=(EditText)findViewById(R.id.editText2);
        et3=(EditText)findViewById(R.id.editText3);
        et4=(EditText)findViewById(R.id.editText4);
        et5=(EditText)findViewById(R.id.editText5);
        b1=(Button)findViewById(R.id.button);
    }
    public void signup (View view)
    {
        Bitmap bitmap=((BitmapDrawable)iv1.getDrawable()).getBitmap();
        ByteArrayOutputStream stream=new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100,stream);
        byteArray=stream.toByteArray();
        username=et1.getText().toString();
        password=et2.getText().toString();
        email=et3.getText().toString();
        mobil=et4.getText().toString();
        pincode=et5.getText().toString();
        dbh=new DataBaseHelper(this);
       boolean result= dbh.signup(byteArray,username,password,email,mobil,pincode);
       if (result==true)
       {
           Intent intent=new Intent(getApplicationContext(),profile.class);
           intent.putExtra("username",username);
           startActivity(intent);
       }else {
           Toast.makeText(getApplicationContext(),"Fail",Toast.LENGTH_LONG).show();
       }

    }
}
