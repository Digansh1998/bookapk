package com.example.book;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.view.View;

import java.util.ArrayList;

public class DataBaseHelper extends SQLiteOpenHelper {
    private static final String db_name = "book.db";
    private static final String t_name = "list";
    private static final String t_name2="login";
    SQLiteDatabase db;

    public DataBaseHelper(Context context) {
        super(context, db_name, null, 1);
        db = this.getWritableDatabase();
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + t_name + "(BookName TEXT,BookDescription TEXT,Categories TEXT,Bookimage blob)");
        db.execSQL("CREATE TABLE " + t_name2 + " (Personimage blob,Username TEXT,Password TEXT,Email TEXT,Mobileno TEXT,Pincode TEXT)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + t_name);
        db.execSQL("DROP TABLE IF EXISTS " +t_name2);
        onCreate(db);
    }
    public boolean insertdata (String bookname,String bookdes,String categerie,byte[] img)
    {
        db=this.getWritableDatabase();
        ContentValues cv=new ContentValues();
        cv.put("BookName",bookname);
        cv.put("BookDescription",bookdes);
        cv.put("Categories",categerie);
        cv.put("Bookimage",img);

        long result=db.insert(t_name,null,cv);
        if (result== -1)
        {
            return false;
        }else {
            return  true;
        }
    }
    public Cursor listdata()
    {

        db=this.getWritableDatabase();
        String SQL="SELECT * FROM list";
        Cursor cursor=db.rawQuery(SQL,null);
               return cursor;
    }
public Cursor selectdata(String Bname)
{
    db=this.getWritableDatabase();
        String SQL="SElECT * FROM list WHERE BookName='"+Bname+"'";
        Cursor cursor=db.rawQuery(SQL,null);
        return cursor;
}
public Cursor Categorey(String Categry)
{
    db=this.getWritableDatabase();
    String Sql="SELECT * FROM LIST WHERE Categories='"+Categry+"'";
    Cursor cursor=db.rawQuery(Sql,null);
    return cursor;
}
public boolean signup (byte[]img,String username,String password,String email,String mobil,String pincode)
{
    db=this.getWritableDatabase();
    ContentValues cv=new ContentValues();
    cv.put("Personimage",img);
    cv.put("Username",username);
    cv.put("Password",password);
    cv.put("Email",email);
    cv.put("Mobileno",mobil);
    cv.put("Pincode",pincode);

    long result=db.insert(t_name2,null,cv);
    if (result==-1)
    {
        return false;
    }else {
        return true;
    }
}
public Cursor login (String username,String password)
{
    db=this.getWritableDatabase();
    String Sql ="SELECT * FROM login WHERE Username='"+username+"' and Password='"+password+"'";
    Cursor cursor=db.rawQuery(Sql,null);

    return cursor;
}
public Cursor profil (String username)
{
    db=this.getWritableDatabase();
    String Sql="SELECT * FROM login WHERE Username='"+username+"'";
    Cursor cursor=db.rawQuery(Sql,null);
    return  cursor;
}




}
