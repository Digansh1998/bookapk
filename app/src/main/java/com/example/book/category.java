package com.example.book;

import android.content.Intent;
import android.database.Cursor;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import java.util.ArrayList;

public class category extends AppCompatActivity {
   DataBaseHelper dbh;
    Bundle bundle;
    ListView lv1;
    String Categoryname;
    Cursor cursor;
    ArrayList<SingleRow> Acategory;
    ArrayList Listner;
    MyAdaptar adaptar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        bundle=getIntent().getExtras();
        Categoryname=bundle.get("cate").toString();
        Toast.makeText(getApplicationContext(),Categoryname+"",Toast.LENGTH_LONG).show();
        lv1=(ListView)findViewById(R.id.lv1);
        dbh=new DataBaseHelper(this);
        cursor=dbh.Categorey(Categoryname);
        Acategory=new ArrayList<SingleRow>();
        SingleRow singleRow;
        Listner=new ArrayList();
        while (cursor.moveToNext())
        {
            String name=cursor.getString(0);
            byte[]image=cursor.getBlob(3);
            Listner.add(cursor.getString(0));


            singleRow = new SingleRow(name, image);
            Acategory.add(singleRow);
        }
        adaptar=new MyAdaptar(this,Acategory);
        lv1.setAdapter(adaptar);
        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent=new Intent(category.this,list.class);
                intent.putExtra("Position",Listner.get(position).toString());
                startActivity(intent);
            }
        });
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.search,menu);
        MenuItem item=menu.findItem(R.id.search);


        final SearchView searchView=(SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adaptar.getFilter().filter(newText);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }
}
