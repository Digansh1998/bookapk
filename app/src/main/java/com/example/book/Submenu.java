package com.example.book;

public class Submenu {


        public String menuName, categoriesname;
        public boolean hasChildren, isGroup;

        public Submenu(String menuName, boolean isGroup, boolean hasChildren, String categoriesname) {

            this.menuName = menuName;
            this.categoriesname = categoriesname;
            this.isGroup = isGroup;
            this.hasChildren = hasChildren;
        }

    }
