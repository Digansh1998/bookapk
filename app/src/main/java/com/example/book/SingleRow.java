package com.example.book;

public class SingleRow {
    String name;
    byte[] image;
    public SingleRow(String name, byte[] image)
    {
        this.name=name;
        this.image=image;

    }



    public String getName(){return name;}
    public byte[] getImage(){return image;}
    public void setName (String name) {this.name=name;}
    public void setImage(byte[] image){this.image=image;}
}
