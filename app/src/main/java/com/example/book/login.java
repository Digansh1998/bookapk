package com.example.book;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class login extends AppCompatActivity {
    DataBaseHelper dbh;
    EditText e1,e2;
    Button b1;
    String p,q;
    Cursor cursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        e1=(EditText)findViewById(R.id.usn);
        e2=(EditText)findViewById(R.id.pass);
        b1=(Button)findViewById(R.id.signup);

    }
    public void sinup (View view)
    {
        dbh=new DataBaseHelper(this);
        p=e1.getText().toString();
        q=e2.getText().toString();
        cursor=dbh.login(p,q);
        if (cursor.moveToNext())
        {
            Intent intent=new Intent(getApplicationContext(),profile.class);
            intent.putExtra("username",p);
            startActivity(intent);
        }else {
            Toast.makeText(getApplicationContext(),"Enter Valid Username Or Password ",Toast.LENGTH_LONG).show();
        }

    }
}
