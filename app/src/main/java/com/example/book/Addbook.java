package com.example.book;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;

public class Addbook extends AppCompatActivity {
    DataBaseHelper dbh;
    ImageView iv1;
    ImageButton ib1;
    EditText et1,et2;
    String Bookname,Bookdes,cname="";
    byte [] byteArray;
    Bitmap image;
    AdapterView.AdapterContextMenuInfo info;
    final int Camera=1888,Gallery=999;
    Spinner sp1;
ArrayList arrayList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addbook);
        iv1=(ImageView)findViewById(R.id.iv1);
        ib1=(ImageButton)findViewById(R.id.ib1);
        et1=(EditText)findViewById(R.id.et1);
        et2=(EditText)findViewById(R.id.et2);
        sp1=(Spinner)findViewById(R.id.sp1);
        arrayList=new ArrayList();
        arrayList.add("Categories");
        arrayList.add("Art and Craft");
       arrayList.add("Comics");
        arrayList.add("Cooking");
        arrayList.add("Health and Fitness");
        arrayList.add("Spiritual");
        arrayList.add("Study");
        arrayList.add("Architecture");
        arrayList.add("Medical");
        arrayList.add("Novel");
        ArrayAdapter adapter=new ArrayAdapter(this,android.R.layout.simple_list_item_1,arrayList);
        sp1.setAdapter(adapter);

        sp1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(arrayList.get(position).equals("Art and Craft"))
                {
                    cname="Art and Craft";
                }else if (arrayList.get(position).equals("Comics"))
                {
                    cname="Comics";
                }else if (arrayList.get(position).equals("Cooking"))
                {
                    cname="Cooking";
                }else if (arrayList.get(position).equals("Health and Fitness"))
                {
                    cname="Health and Fitness";
                }else if (arrayList.get(position).equals("Spiritual"))
                {
                    cname="Spiritual";
                }else if (arrayList.get(position).equals("Study"))
                {
                    cname="Study";
                }else if (arrayList.get(position).equals("Architecture"))
                {
                    cname="Architecture";
                }else if (arrayList.get(position).equals("Medical"))
                {
                    cname="Medical";
                }else if (arrayList.get(position).equals("Novel"))
                {
                    cname="Novel";
                }
                else if (arrayList.get(position).equals("Categories"))
                {
                    Toast.makeText(getApplicationContext(),"Please Select Category",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.upload,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Bookname=et1.getText().toString();
        Bookdes=et2.getText().toString();

        switch (item.getItemId())
        {
            case R.id.add1:
                Bitmap bitmap=((BitmapDrawable)iv1.getDrawable()).getBitmap();
                ByteArrayOutputStream stream=new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG,100,stream);
                byteArray=stream.toByteArray();
                dbh=new DataBaseHelper(this);
                boolean result=dbh.insertdata(Bookname,Bookdes,cname,byteArray);
                if (result==true)
                {
                    Intent intent=new Intent(Addbook.this,MainActivity.class);
                    startActivity(intent);
                    Toast.makeText(getApplicationContext(),"successfully Upload",Toast.LENGTH_LONG).show();
                    Toast.makeText(getApplicationContext(),cname+"",Toast.LENGTH_LONG).show();
                    cname="";
                }else {
                    Toast.makeText(getApplicationContext(),"Not Inserted",Toast.LENGTH_LONG).show();
                }
                return true;
                default:
                    return super.onOptionsItemSelected(item);

        }

    }
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater =getMenuInflater();
        inflater.inflate(R.menu.imagebutton,menu);
        info=(AdapterView.AdapterContextMenuInfo)menuInfo;
        menu.setHeaderTitle("Choose Photo");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getItemId()==R.id.take1)
        {
            //Toast.makeText(getApplicationContext(),"Take Photo",Toast.LENGTH_LONG).show();
            Intent camIntent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(camIntent,Camera);

        }
        else if (item.getItemId()==R.id.choose1)
        {
            // Toast.makeText(getApplicationContext(),"Choose Photo",Toast.LENGTH_LONG).show();
            ActivityCompat.requestPermissions(Addbook.this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},Gallery);
        }
        return super.onContextItemSelected(item);
    }
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode==Gallery)
        {
            if(grantResults.length>0&&grantResults[0]== PackageManager.PERMISSION_GRANTED)
            {
                Intent inn=new Intent(Intent.ACTION_PICK);
                inn.setType("image/*");
                startActivityForResult(inn,Gallery);
            }
        }


    }
    public void Button(View  view)
    {
        registerForContextMenu(view);
        openContextMenu(view);
        unregisterForContextMenu(view);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==Camera) {
            image = (Bitmap) data.getExtras().get("data");
            iv1.setImageBitmap(image);
        }
        else if (requestCode==Gallery)
        {
            Uri uri =data.getData();
            try {
                InputStream inputStream=getContentResolver().openInputStream(uri);
                image= BitmapFactory.decodeStream(inputStream);
                iv1.setImageBitmap(image);


            }catch (Exception e)
            {

            }
        }
    }

}
