package com.example.book;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

public class MyAdaptar extends BaseAdapter implements Filterable {
    Context c;
    ArrayList<SingleRow> OArray,toarray;
    CustomFilter cs;

    public MyAdaptar(Context c,ArrayList<SingleRow> OArray)
    {
        this.c=c;
        this.OArray=OArray;
        this.toarray=OArray;

    }


    @Override
    public int getCount() {
        return OArray.size();
    }

    @Override
    public Object getItem(int position) {
        return OArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater=(LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View Roe=inflater.inflate(R.layout.imabu,null);
        TextView t1=(TextView)Roe.findViewById(R.id.tv1);
       ImageView iv1=(ImageView) Roe.findViewById(R.id.iv1);
       t1.setText(OArray.get(position).getName());
       byte[] imge=(OArray.get(position).getImage());
       Bitmap bm=BitmapFactory.decodeByteArray((imge),0,(imge).length);
       iv1.setImageBitmap(bm);


        return Roe;
    }
    @Override
    public Filter getFilter() {
        if (cs==null)
        {
            cs=new CustomFilter();
        }
        return cs;
    }
    class CustomFilter extends Filter
    {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results=new FilterResults();
            if (constraint!=null&& constraint.length()>0)
            {
                constraint=constraint.toString().toUpperCase();
                ArrayList<SingleRow> filters=new ArrayList<>();
                for (int i=0;i<toarray.size();i++)
                {
                    if (toarray.get(i).getName().toUpperCase().contains(constraint))
                    {
                        SingleRow singleRow=new SingleRow(toarray.get(i).getName(),toarray.get(i).getImage());
                        filters.add(singleRow);
                    }
                }
                results.count=filters.size();
                results.values=filters;

            }
            else {
                results.count=toarray.size();
                results.values=toarray;
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            OArray=(ArrayList<SingleRow>)results.values;
            notifyDataSetChanged();
        }
    }
}
