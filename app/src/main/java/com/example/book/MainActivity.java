package com.example.book;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity  implements TextWatcher {
    ListView l1;
    DataBaseHelper dbh;
    Cursor cursor;
    FloatingActionButton fab1;


    //String name[]={"Book1","Book2","Book3","Book4","Book5"};
    //int image[]={R.drawable.a1,R.drawable.b2,R.drawable.c1,R.drawable.d1,R.drawable.e1};
    ArrayList<SingleRow> mylist;
    MyAdaptar adaptar;
    ArrayList Aname;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        WindowManager.LayoutParams.FLAG_FULLSCREEN);
        l1 = findViewById(R.id.lv1);
        dbh=new DataBaseHelper(this);
        cursor=dbh.listdata();

        mylist = new ArrayList<>();
        SingleRow singleRow;
        /*for (int i = 0; i < name.length; i++) {
            singleRow = new SingleRow(name[i], image[i]);
            mylist.add(singleRow);
        }*/
        Aname=new ArrayList();
        while (cursor.moveToNext())

        {
           String name=cursor.getString(0);
           byte[]image=cursor.getBlob(3);
           Aname.add(cursor.getString(0));


            singleRow = new SingleRow(name, image);
            mylist.add(singleRow);

        }
        adaptar=new MyAdaptar(this,mylist);
        l1.setAdapter(adaptar);
       l1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
           @Override
           public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               Intent intent=new Intent(MainActivity.this,list.class);
intent.putExtra("Position",Aname.get(position).toString());
               startActivity(intent);
              // Toast.makeText(getApplicationContext(),"Toast",Toast.LENGTH_LONG).show();
           }
       });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.search,menu);
        MenuItem item=menu.findItem(R.id.search);


        final SearchView searchView=(SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adaptar.getFilter().filter(newText);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        this.adaptar.getFilter().filter(s);

    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {

default:
    return super.onOptionsItemSelected(item);

        }


    }
    public void floatingactionbutton(View view)

    {
        fab1=(FloatingActionButton)findViewById(R.id.fab1);
        Intent intent=new Intent(MainActivity.this,Addbook.class);
        startActivity(intent);
    }

}

